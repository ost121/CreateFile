﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CreateFile
{
    class Program
    {
        static void Main(string[] args)
        {
            //Имя каталога файла и имя файла
            string path1Name = @"d:\Новая папка";
            string pathName = @"d:\Новая папка\Новый файл.txt";
            //Создаём папку для файла
            Directory.CreateDirectory(path1Name);
            //Создаём массив строк, вводимых в файл
            string[] anyText = new string[3];
            //Находим имя директории, затем - приложения
            string directory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            anyText[2] = Path.GetFileName(directory);
            //Получаем текущую дату(1 строка)
            anyText[0] = DateTime.Now.ToString();
            //Получаем директорию приложения(2 строка)
            anyText[1] = Environment.CurrentDirectory;

            //Если файл не существует, создать и записать в него 3 строки
            if (!File.Exists(path1Name))
            {
                File.WriteAllLines(pathName, anyText);
            }
            Console.WriteLine("Файл был создан. \nНажмите любую клавишу, чтобы удалить файл...");
            Console.ReadLine();
            File.Delete(@"d:\Новая папка\Новый файл.txt");
            Console.WriteLine("Файл был удалён.");
        }
    }
}
